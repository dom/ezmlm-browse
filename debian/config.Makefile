config: warn config.pyc

config.pyc: config.py
	PYTHONPATH=/usr/lib/ezmlm-browse python -c '__import__("config")'

clean:
	rm -f config.pyc

htmlobsolete=\
  footer.inc header.inc menu.html monthbydate.html monthbythread.html \
  post.html postform.html reply.html replyform.html search.html \
  searchform.html showmsg.html showthread.html thread.html threadindex.html

warn:
	@test -d ./html || sh -cx '! : directory ./html does not exist.'
	@for i in $(htmlobsolete); do \
	  test -e html/$$i || continue; \
	  echo "./html/$$i is obsolete, and can be removed."; \
	  echo "Run 'make htmlcleanup' to remove obsolete html templates." \
	    >.'{obsolete}'; \
	done
	@test ! -e .'{obsolete}' || \
	  (echo; cat .'{obsolete}'; echo; rm -f .'{obsolete}')
htmlcleanup:
	@test -d ./html || sh -cx '! : directory ./html does not exist.'
	@for i in $(htmlobsolete); do \
	  test -e html/$$i || continue; \
	  sh -cx "rm -f ./html/$$i"; \
	done
